// [NEW SECTION] What are OBJECTS?

// => An OBJECT is a collection and/or functionality.

// ANALOGY:
      // if CSS ===> everything is a "Box"
      // JS ===> Most things are objects
      // Example: Cellphone -> is an object in the real world.
            // it has its own properties(color, weight, unit)
            // it has its own functions (open, close, send message, and etc.)

// [HOW TO CREATE/INSTANTIATE AN OBJECT IN JS]
      // => THERE ARE 2 WAYS TO CREATE AN OBJECT
      // 1. Curly Braces
            // let cellphone = {}
      // 2. Object Constructor / Object Initializer
            // let cellphone = Object()

// let cellphone = {
//       color: 'White',
//       weight: '115 grams',
//       // an object can also contain functions
//       alarm: function() {
//             console.log('Alarm is Buzzing');
//       },
//       ring: function () {
//             console.log('Cellphone is Ringing')
//       }
// }
let cellphone = Object({
      color: 'White',
      weight: '115 grams',
      // an object can also contain functions
      alarm: function() {
            console.log('Alarm is Buzzing');
      },
      ring: function () {
            console.log('Cellphone is Ringing')
      }
})
console.log(cellphone);

// HOW TO CREATE AN OBJECT AS A BLUEPRINT?
      // => We can create reusable functions that will create several objects that have the same Data Structures.

      // this approach is very uselful in creating multiple instances/duplicates/copies of the same object.

// SYNTAX: function desiredBlueprintName(argN) {
      // this.argN = valueNiArgN
// }

function Laptop(name, manufactureDate, color) {
      this.pangalan = name;
      this.createdOn = manufactureDate;
      this.kulay = color;
}     
      // this => keyword will allow us to assign a new object properties by associating the received values.

// new => this keyword is used to create an instance of a new object
let item1 = new Laptop('Lenovo', '2008', 'black');
let item2 = new Laptop('Asus', '2015', 'Pink');

console.log(item1);
console.log(item2);

// Create a function that will allow us to create multiple instance of a Pokemon.
function PokemonAnatomy(name, type, level) {
      this.pokemonName = name;
      this.pokemonType = type;
      this.pokemonLevel = level;
      this.pokeHealth = 80 * level;
      this.attack = function() {
            console.log('Pokemon Tackle');
      }
}

let pikachu = new PokemonAnatomy('Pikachu', 'Electric', 3)
let ratata = new PokemonAnatomy('Ratata', 'Ground', 2)
let onyx = new PokemonAnatomy('Onyx', 'Rock', 8)
let meowth = new PokemonAnatomy('Meowth', 'Normal', 9)
let snorlax = new PokemonAnatomy('Snorlax', 'Normal', 9)

console.log(pikachu);

// [ACCESS ELEMENTS/PROPERTIES INSIDE AN OBJECT]
      // 1. Dot Notation (.)
            console.log(pikachu.pokeHealth);
      // 2. Square Brackets ([""])
            console.log(snorlax['pokemonType']);